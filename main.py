from tkinter import *
import math

# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 1
SHORT_BREAK_MIN = 1
LONG_BREAK_MIN = 1
reps = 0
timer = None


# ---------------------------- TIMER RESET ------------------------------- #
def reset_timer():
    global timer
    global reps
    reps = 0
    window.after_cancel(timer)

    #reset time to 00:00
    canvas.itemconfig(timer_text,text="00:00")
    #reset title to Timer
    title_lb.config(text="Timer", font=(FONT_NAME, 44), bg=YELLOW, fg=GREEN)
    #reset check marks
    check_lb.config(text="", font=("Arial", 14, "bold"))
    #enable start button
    button.config(state=ACTIVE)
    button1.config(state=DISABLED)

# ---------------------------- TIMER MECHANISM ------------------------------- #
def start_timer():
    button.config(state=DISABLED)
    button1.config(state=ACTIVE)
    global reps
    reps += 1
    timer_sec = 0
    if reps in [2, 4, 6]:
        title_lb.config(text="Break", fg=RED)
        timer_sec = SHORT_BREAK_MIN * 60
    elif reps == 8:
        title_lb.config(text="Break", fg=PINK)
        timer_sec = LONG_BREAK_MIN * 60
        reps = 0
    else:
        title_lb.config(text="Work", fg=GREEN)
        timer_sec = WORK_MIN * 60

    count_down(timer_sec)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #

def count_down(count):
    count_min = math.floor(count / 60)
    count_sec = count % 60
    if count_sec < 10:
        count_sec = f"0{count_sec}"

    if count_min < 10:
        count_min = f"0{count_min}"

    canvas.itemconfig(timer_text, text=f"{count_min}:{count_sec}")

    if count > 0:
        global timer
        timer = window.after(1000, count_down, count - 1)
    else:
        start_timer()
        checks = ""
        for _ in range(math.floor(reps / 2)):
            checks += "✔"

        check_lb.config(text=checks, font=("Arial", 14, "bold"))


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Pomodoro")
window.config(padx=100, pady=50, bg=YELLOW)

title_lb = Label(text="Timer", font=(FONT_NAME, 44), bg=YELLOW, fg=GREEN)
title_lb.grid(row=0, column=1)

canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)
tomato_img = PhotoImage(file='tomato.png')
canvas.create_image(100, 112, image=tomato_img)
timer_text = canvas.create_text(100, 130, text='00:00', fill="white", font=(FONT_NAME, 35, "bold"))
canvas.grid(row=1, column=1)

button = Button(text='Start', font=("Arial", 12), highlightthickness=0, command=start_timer)
button.grid(row=2, column=0)

button1 = Button(text='Reset', font=("Arial", 12), highlightthickness=0, state=DISABLED, command=reset_timer)
button1.grid(row=2, column=2)

check_lb = Label(background=YELLOW, foreground=GREEN)
check_lb.grid(row=3, column=1)

window.mainloop()
